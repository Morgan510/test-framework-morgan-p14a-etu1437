 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.net.MalformedURLException;
import java.util.HashMap;
import model.ModelView;
import utilitaire.MethodUrl;

/**
 *
 * @author morga
 */

public class Admin{
    private String usr;
    private String mdp;

    public Admin() {
    }

    public Admin(String usr, String mdp) {
        this.usr = usr;
        this.mdp = mdp;
    }
    
    public String getUsr() {
        return usr;
    }
    
    public void setUsr(String usr) {
        this.usr = usr;
    }
    
    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
    
    @MethodUrl(url="listerAdmin")
    public ModelView listerAdmin() throws MalformedURLException{
        Admin[] admin = new Admin[2];
        admin[0] = new Admin();
        admin[1] = new Admin();
        admin[0].usr = "momo";
        admin[0].mdp = "12345";
        admin[1].usr = "morgan";
        admin[1].mdp = "2469";
        String url = "listerAdmin.jsp";
        HashMap<String,Admin[]> hm = new HashMap();
        hm.put("listeAdmin", admin);
        
        ModelView mv = new ModelView();
        mv.setUrl(url);
        mv.setDonnee(hm);
        
        return mv;
    }
    
    @MethodUrl(url="saveAdmin")
    public ModelView saveAdmin() throws MalformedURLException{
        
        String url = "saveAdmin.jsp";
        HashMap<String,Admin[]> hm = new HashMap();
        ModelView mv = new ModelView();
        mv.setUrl(url);
        mv.setDonnee(hm);
        
        return mv;
    }
}
